from __future__ import annotations
from abc import ABC, abstractmethod

class Bike_Creator(ABC):  #Template for creating bikes.
    @abstractmethod
    def factory_method(self):
        ...

    def create_bike(self) -> str:
        bike = self.factory_method()
        result = bike.max_speed()
        return result

class Road_Bike_Creator(Bike_Creator):
    def factory_method(self) -> Bike:
        print("Road bike is created")
        return RoadBike()

class Mountain_Bike_Creator(Bike_Creator):
    def factory_method(self) -> Bike:
        print("Mountain bike is created")
        return MountainBike()

class Bike(ABC):
    @abstractmethod
    def max_speed(self) -> str:
        ...

class RoadBike(Bike):
    def max_speed(self) -> str:
        return "Maximum speed: 19.13 km/h"

class MountainBike(Bike):
    def max_speed(self) -> float:
        return "Maximum speed: 32.19 km/h"

def client_code(bike: Bike) -> None:
    print(bike.create_bike())
    
road_bike1=Road_Bike_Creator()    
client_code(road_bike1)    


