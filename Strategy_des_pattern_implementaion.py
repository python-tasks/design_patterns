from __future__ import annotations
from abc import ABC, abstractmethod


class Sorter():
    
    def __init__(self, strategy: Strategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def data_sorting(self,data) -> None:
        print(f"Sorting data using the strategy '{type(self._strategy).__name__}'")
        result = self._strategy.do_algorithm(data)
        print(",".join(result))

class Strategy(ABC):
    @abstractmethod
    def do_algorithm(self, data: list):
        pass

class SortingStrategy(Strategy):
    def do_algorithm(self, data: list) -> list:
        return sorted(data)


class RevesingStrategy(Strategy):
    def do_algorithm(self, data: list) ->list:
        return reversed(sorted(data))

sort_strategy = Sorter(SortingStrategy())
print("Client: Strategy is set to sorting.")
ls=["a", "b",  "d", "e","c"]
sort_strategy.data_sorting(ls)
print()

print("Client: Strategy is set to reverse sorting.")
sort_strategy.strategy = RevesingStrategy()
sort_strategy.data_sorting(ls)
