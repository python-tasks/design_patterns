from __future__ import annotations
from abc import ABC, abstractmethod

class ATMCashSupplyChain(ABC):
    
    @abstractmethod
    def next_section(self, section: ATMCashSupplyChain) -> ATMCashSupplyChain:
        pass

    @abstractmethod
    def handle(self, request,string:str,total:int) -> int:
        pass

class AbstractSection(ATMCashSupplyChain):
    
    def next_section(self, section: ATMCashSupplyChain) -> ATMCashSupplyChain:
        self._next_section = section
        return section

    #@abstractmethod
    def handle(self, request: int, string:str, total:int) -> str:
        if hasattr(self, '_next_section'):
            return self._next_section.handle(request, string, total)
        return request

class Section_100Dollar(AbstractSection):
    def handle(self, request: int, string:str, total:int) -> str:
        if request==100:
            total = request
            string="1*100$"
        elif (request-total)>100:
            rem=request//100
            total += rem * 100
            string=f"{rem}*100$"
            if (request- total)>= 50:
                return self.next_section(Section_50Dollar()).handle(request,string, total)
            elif (request- total) >= 20:
                return self.next_section(Section_20Dollar()).handle(request,string, total)
            elif (request- total)>= 10:
                return self.next_section(Section_10Dollar()).handle(request,string, total)
            elif (request- total) >= 5:
                return self.next_section(Section_5Dollar()).handle(request,string, total)
        return f"{total}, {string}"

class Section_50Dollar(AbstractSection):
    def handle(self, request: int, string, total) -> str:
        string+="+1*50$"
        total+=50
        if (request - total) >=20:
            return self.next_section(Section_20Dollar()).handle(request,string, total)
    
        elif (request - total) >=10:
            return self.next_section(Section_10Dollar()).handle(request,string, total)
        else:
            return self.next_section(Section_5Dollar()).handle(request,string, total)
        return f"{total}, {string}"

class Section_20Dollar(AbstractSection):
    def handle(self, request: int, string, total) -> str:
        if (request-total)==20:
            total += 20
            string+="+1*20$"
        elif (request-total)==40:
            total += 40
            string+="+2*20$"
            
        else:  
            total += 20
            string+="+ 1*20$"
            if (request - total) >= 10:
                return self.next_section(Section_10Dollar()).handle(request,string,total)
            elif (request - total) >= 5:
                return self.next_section(Section_5Dollar()).handle(request,string, total)
        return f"{total}, {string}"

class Section_10Dollar(AbstractSection):
    def handle(self, request: int, string, total) -> str:
        total += 10
        string+=' + 1*10$'
        if (request - total) >=5:
            return self.next_section(Section_5Dollar()).handle(request,string, total)
        return f"{total}, {string}"

class Section_5Dollar(AbstractSection):
    def handle(self, request: int,string, total) -> str:
        total+=5
        string+=' + 1*5$'
        return f"{total}, {string}"


def client_code(request) -> str:
    total = 0
    max_amount=10000
    string=""
    if request % 5 != 0 or request>max_amount:
        return "Wrong amount !!!"
    elif request >= 100:
        return Section_100Dollar().handle(request,string="",total=0)
    elif request >= 50:
        return Section_50Dollar().handle(request,string="",total=0)
    elif request >= 20:
        return Section_20Dollar().handle(request,string="",total=0)
    elif request >= 10:
        return Section_10Dollar().handle(request,string="",total=0) 
    elif request >= 5:
        return Section_5Dollar().handle(request,string="",total=0) 
    else:
        return "Invalid request"

print(client_code(1115))
