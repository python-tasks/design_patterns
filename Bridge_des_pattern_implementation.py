from abc import ABC, abstractmethod

class Color(ABC):
    @abstractmethod
    def get_color(self) -> str:
        pass

class Red(Color):
    def get_color(self) -> str:
        return "red"

class Blue(Color):
    def get_color(self) -> str:
        return "blue"


class Figure(ABC):
    def __init__(self, color: Color) -> None:
        self._color = color

    @abstractmethod
    def make_figure(self) -> str:
        pass


class Sphere(Figure):
    def make_figure(self) -> str:
        return f"Sphere in {self._color.get_color()} win created"

class Cube(Figure):
    def make_figure(self) -> str:
        return f"Cube in {self._color.get_color()} is created"


red = Red()
blue = Blue()

sphere = Sphere(red)
print(sphere.make_figure())

cube = Cube(blue)
print(cube.make_figure())

