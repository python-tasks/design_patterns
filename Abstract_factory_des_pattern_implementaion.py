from __future__ import annotations
from abc import ABC, abstractmethod

class VehicleFactory(ABC):
    @abstractmethod
    def create_passenger_vehicle(self) -> PassengerVehicle:
        pass

    @abstractmethod
    def create_cargo_vehicle(self) -> CargoVehicle:
        pass
    
class CarFactory(VehicleFactory):
    def create_passenger_vehicle(self) -> PassengerVehicle:
        return PassengerCar()

    def create_cargo_vehicle(self) -> CargoVehicle:
        return CargoCar()

class ShipFactory(VehicleFactory):
    def create_passenger_vehicle(self) -> PassengerVehicle:
        return PassengerShip()

    def create_cargo_vehicle(self) -> CargoVehicle:
        return CargoShip()
        
class PlaneFactory(VehicleFactory):
    def create_passenger_vehicle(self) -> PassengerVehicle:
        return PassengerPlane()

    def create_cargo_vehicle(self) -> CargoVehicle:
        return CargoPlane()        

class PassengerVehicle(ABC):
    @abstractmethod
    def seats_number(self) -> str:
        ...
    @abstractmethod
    def fueling (self)-> str:
        ...

class PassengerCar(PassengerVehicle):
    def seats_number(self) -> str:
        return "The number of the car seats: 4"
    def fueling (self)-> str:
        return "fueling type: 'Gasoline/Petrol'"

class PassengerPlane(PassengerVehicle):
    def seats_number(self) -> str:
        return "The number of the plane seats: 200"
    def fueling (self)-> str:
        return "fueling type: 'Diesel'"
    
class PassengerShip(PassengerVehicle):
    def seats_number(self) -> str:
        return "The number of the ship seats: 100"
    def fueling (self)-> str:
        return "fueling type: 'Diesel Oil'"    


class CargoVehicle(ABC):
    @abstractmethod
    def max_load(self) -> None:
        ...
    
    @abstractmethod
    def fueling_type(self) -> None:
        ...

class CargoCar(CargoVehicle):
    def max_load(self) -> str:
        return "The maximum load of the car: 6.5 Ton"

    def fueling_type(self, pass_vehicle: PassengerVehicle) -> str:
        result = pass_vehicle.fueling()
        return f"Cargo car ({result})"

class CargoPlane(CargoVehicle):
    def max_load(self) -> str:
        return "The maximum load of the plane: 50 Ton"

    def fueling_type(self, pass_vehicle: PassengerVehicle) -> str:
        result = pass_vehicle.fueling()
        return f"Cargo plane ({result})"

class CargoShip(CargoVehicle):
    def max_load(self) -> str:
        return "The maximum load of the ship: 28 Ton"

    def fueling_type(self, pass_vehicle: PassengerVehicle) -> str:
        result = pass_vehicle.fueling()
        return f"Cargo ship ({result})"

def vehicle(factory: VehicleFactory) -> None:
    
    pass_vehicle = factory.create_passenger_vehicle()
    cargo_vehicle = factory.create_cargo_vehicle()

    print(f"{cargo_vehicle.max_load()}")
    print(f"{cargo_vehicle.fueling_type(pass_vehicle)}")
    
car_vehicle=CarFactory()    
print(car_vehicle.create_passenger_vehicle().seats_number())
print(car_vehicle.create_passenger_vehicle().fueling())

ship_vehicle=ShipFactory()
vehicle(ship_vehicle)
