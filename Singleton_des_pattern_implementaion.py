class Singleton:
    instance=None
    def __new__(cls,*args, **kwargs):
        if cls.instance is None:
            cls.instance=super().__new__(cls)
            cls.instance.value=15
        return cls.instance

s=Singleton() 
print(s.value)
s1=Singleton()
print(s is s1)


